from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import (
    QMessageBox)
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtCore import *
from ui_communicationdialog import Ui_communicationdialog

import json
import os
import pymysql


def detect(ip):
    ret = os.system("ping "+ip+" -n 1")
    if ret == 0:
        print("ip可以ping通！")
        return True
    else:
        print("ip不可以ping通！")
        return False


class QmyCommunicationDialog(QDialog):
    detect_model_configActionEnable = pyqtSignal(bool)
    change_model_config = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_communicationdialog()
        self.ui.setupUi(self)
        # 固定窗口
        self.setWindowFlag(Qt.WindowStaysOnTopHint)

    def setInitial(self):
        self.config_file = 'config/communication.json'
        self.config = json.load(open(self.config_file, 'r', encoding='utf-8'))
        self.plc_ip = self.config["plc_ip"]
        self.plc_start_point = self.config["plc_start_point"]
        self.plc_finish_point = self.config["plc_finish_point"]
        self.plc_result_point = self.config["plc_result_point"]
        self.database_ip = self.config["database_ip"]
        self.database_port = self.config['database_port']
        self.database_user = self.config['database_user']
        self.database_password = self.config['database_password']
        self.database_name = self.config['database_name']
        self.ui.plc_ip_value.setText(self.plc_ip)
        self.ui.plc_start_point_value.setText(self.plc_start_point)
        self.ui.plc_finish_point_value.setText(self.plc_finish_point)
        self.ui.plc_result_point_value.setText(self.plc_result_point)
        self.ui.database_ip_value.setText(self.database_ip)
        self.ui.database_port_value.setText(self.database_port)
        self.ui.database_user_value.setText(self.database_user)
        self.ui.database_password_value.setText(self.database_password)
        self.ui.database_name_value.setText(self.database_name)

    @pyqtSlot()
    def on_plc_config_savebtn_clicked(self):
        self.new_plc_ip_value = self.ui.plc_ip_value.text()
        self.new_plc_start_point_value = self.ui.plc_start_point_value.text()
        self.new_plc_finish_point_value = self.ui.plc_finish_point_value.text()
        self.new_plc_result_point_value = self.ui.plc_result_point_value.text()

        self.new_database_ip_value = self.ui.database_ip_value.text()
        self.new_database_port_value = self.ui.database_port_value.text()
        self.new_database_user_value = self.ui.database_user_value.text()
        self.new_database_password_value = self.ui.database_password_value.text()
        self.new_database_name_value = self.ui.database_name_value.text()
        self.new_config = {"plc_ip": self.new_plc_ip_value,
                           "plc_start_point": self.new_plc_start_point_value,
                           "plc_finish_point": self.new_plc_finish_point_value,
                           "plc_result_point": self.new_plc_result_point_value,
                           "database_ip": self.new_database_ip_value,
                           "database_port": self.new_database_port_value,
                           "database_user": self.new_database_user_value,
                           "database_password": self.new_database_password_value,
                           "database_name": self.new_database_name_value
                           }
        new_json = json.dumps(self.new_config, ensure_ascii=False, indent=2)
        with open(self.config_file, 'w', encoding='utf-8') as f:
            f.write(new_json)
        print("PLC配置保存到通讯配置文件成功！")
        QMessageBox.information(self, "提示！", "PLC配置保存到通讯配置文件成功！!")

    @pyqtSlot()
    def on_database_config_savebtn_clicked(self):
        self.new_plc_ip_value = self.ui.plc_ip_value.text()
        self.new_plc_start_point_value = self.ui.plc_start_point_value.text()
        self.new_plc_finish_point_value = self.ui.plc_finish_point_value.text()
        self.new_plc_result_point_value = self.ui.plc_result_point_value.text()

        self.new_database_ip_value = self.ui.database_ip_value.text()
        self.new_database_port_value = self.ui.database_port_value.text()
        self.new_database_user_value = self.ui.database_user_value.text()
        self.new_database_password_value = self.ui.database_password_value.text()
        self.new_database_name_value = self.ui.database_name_value.text()
        self.new_config = {"plc_ip": self.new_plc_ip_value,
                           "plc_start_point": self.new_plc_start_point_value,
                           "plc_finish_point": self.new_plc_finish_point_value,
                           "plc_result_point": self.new_plc_result_point_value,
                           "database_ip": self.new_database_ip_value,
                           "database_port": self.new_database_port_value,
                           "database_user": self.new_database_user_value,
                           "database_password": self.new_database_password_value,
                           "database_name": self.new_database_name_value
                           }
        new_json = json.dumps(self.new_config, ensure_ascii=False, indent=2)
        with open(self.config_file, 'w', encoding='utf-8') as f:
            f.write(new_json)
        print("数据库配置保存到通讯配置文件成功！")
        QMessageBox.information(self, "提示！", "数据库配置保存到通讯配置文件成功！!")

    @pyqtSlot()
    def on_plc_test_commubtn_clicked(self):
        self.on_plc_config_savebtn_clicked()
        print("点击了PLC检测连接!")
        if detect(str(self.plc_ip)) == True:
            print("ip通")
            QMessageBox.information(self, "提示！", "PLC主机测试连接成功!")
        else:
            print("ip不通")
            QMessageBox.information(self, "提示！", "PLC主机测试连接失败!")

    @pyqtSlot()
    def on_database_test_commubtn_clicked(self):
        self.on_database_config_savebtn_clicked()
        print("点击了数据库检测连接!")
        try:
            # 打开数据库连接
            db = pymysql.connect(host=self.new_database_ip_value, user=self.new_database_user_value,
                                 password=self.new_database_password_value, database=self.new_database_name_value)
            cur = db.cursor()
            QMessageBox.information(self, "提示！", "数据库主机测试连接成功!")
            print('数据库连接成功！')
        except:
            print("数据库连接失败:"+str(pymysql.Error))
            QMessageBox.information(
                self, "提示！", '"数据库连接失败:"+str(pymysql.Error)"')
