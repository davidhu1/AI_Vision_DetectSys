#ifndef DETECTMODELCONFIGDIALOG_H
#define DETECTMODELCONFIGDIALOG_H

#include <QDialog>

namespace Ui {
class DetectModelConfigDialog;
}

class DetectModelConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DetectModelConfigDialog(QWidget *parent = nullptr);
    ~DetectModelConfigDialog();

private:
    Ui::DetectModelConfigDialog *ui;
};

#endif // DETECTMODELCONFIGDIALOG_H
