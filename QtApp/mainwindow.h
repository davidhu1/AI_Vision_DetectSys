#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_exposure_horizontalSlider_valueChanged(int value);

    void on_max_det_horizontalSlider_valueChanged(int value);

    void on_parameter_config_triggered();

    void on_save_to_config_pressed();

    void on_comboBox_currentTextChanged(const QString &arg1);

    void on_takePicButton_released();

    void on_detect_model_config_triggered();

    void on_communication_config_triggered();

    void on_save_to_config_clicked();

    void on_auto_run_btn_clicked();

    void on_stop_run_btn_clicked();

    void on_start_detect_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
