#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_exposure_horizontalSlider_valueChanged(int value)
{

}

void MainWindow::on_max_det_horizontalSlider_valueChanged(int value)
{

}


void MainWindow::on_parameter_config_triggered()
{

}


void MainWindow::on_save_to_config_pressed()
{

}

void MainWindow::on_comboBox_currentTextChanged(const QString &arg1)
{

}

void MainWindow::on_takePicButton_released()
{

}

void MainWindow::on_detect_model_config_triggered()
{

}

void MainWindow::on_communication_config_triggered()
{

}


void MainWindow::on_auto_run_btn_clicked()
{

}

void MainWindow::on_stop_run_btn_clicked()
{

}

void MainWindow::on_start_detect_clicked()
{

}
