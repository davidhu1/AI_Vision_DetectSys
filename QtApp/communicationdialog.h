#ifndef COMMUNICATIONDIALOG_H
#define COMMUNICATIONDIALOG_H

#include <QDialog>

namespace Ui {
class communicationdialog;
}

class communicationdialog : public QDialog
{
    Q_OBJECT

public:
    explicit communicationdialog(QWidget *parent = nullptr);
    ~communicationdialog();

private slots:
    void on_plc_test_commubtn_clicked();

    void on_plc_config_savebtn_clicked();

    void on_database_config_savebtn_clicked();

    void on_database_test_commubtn_clicked();

private:
    Ui::communicationdialog *ui;
};

#endif // COMMUNICATIONDIALOG_H
