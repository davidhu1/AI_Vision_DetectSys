#include "detectmodelconfigdialog.h"
#include "ui_detectmodelconfigdialog.h"

DetectModelConfigDialog::DetectModelConfigDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DetectModelConfigDialog)
{
    ui->setupUi(this);
}

DetectModelConfigDialog::~DetectModelConfigDialog()
{
    delete ui;
}
