# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'f:\AI_Vision_DetectSys\QtApp\detectmodelconfigdialog.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_DetectModelConfigDialog(object):
    def setupUi(self, DetectModelConfigDialog):
        DetectModelConfigDialog.setObjectName("DetectModelConfigDialog")
        DetectModelConfigDialog.resize(505, 405)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/130.bmp"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        DetectModelConfigDialog.setWindowIcon(icon)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(DetectModelConfigDialog)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.groupBox = QtWidgets.QGroupBox(DetectModelConfigDialog)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.modelcomboBox = QtWidgets.QComboBox(self.groupBox)
        self.modelcomboBox.setMinimumSize(QtCore.QSize(0, 30))
        self.modelcomboBox.setObjectName("modelcomboBox")
        self.verticalLayout.addWidget(self.modelcomboBox)
        self.setNowModelBtn = QtWidgets.QPushButton(self.groupBox)
        self.setNowModelBtn.setEnabled(False)
        self.setNowModelBtn.setMinimumSize(QtCore.QSize(0, 30))
        self.setNowModelBtn.setStyleSheet("background-color:rgb(146, 189, 108);\n"
"border:1px solid;\n"
"border-radius:10px;")
        self.setNowModelBtn.setObjectName("setNowModelBtn")
        self.verticalLayout.addWidget(self.setNowModelBtn)
        self.verticalLayout_2.addWidget(self.groupBox)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btnOk = QtWidgets.QPushButton(DetectModelConfigDialog)
        self.btnOk.setMinimumSize(QtCore.QSize(0, 30))
        self.btnOk.setObjectName("btnOk")
        self.horizontalLayout.addWidget(self.btnOk)
        self.btnCancle = QtWidgets.QPushButton(DetectModelConfigDialog)
        self.btnCancle.setMinimumSize(QtCore.QSize(0, 30))
        self.btnCancle.setObjectName("btnCancle")
        self.horizontalLayout.addWidget(self.btnCancle)
        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.retranslateUi(DetectModelConfigDialog)
        self.btnOk.clicked.connect(DetectModelConfigDialog.accept) # type: ignore
        self.btnCancle.clicked.connect(DetectModelConfigDialog.reject) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(DetectModelConfigDialog)

    def retranslateUi(self, DetectModelConfigDialog):
        _translate = QtCore.QCoreApplication.translate
        DetectModelConfigDialog.setWindowTitle(_translate("DetectModelConfigDialog", "检测模型配置"))
        self.groupBox.setTitle(_translate("DetectModelConfigDialog", "选择检测模型"))
        self.setNowModelBtn.setText(_translate("DetectModelConfigDialog", "设置为当前模型"))
        self.btnOk.setText(_translate("DetectModelConfigDialog", "确定"))
        self.btnCancle.setText(_translate("DetectModelConfigDialog", "取消"))
import res_rc
