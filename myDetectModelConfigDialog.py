from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import Qt, pyqtSignal
from ui_detectmodelconfigdialog import Ui_DetectModelConfigDialog
import os


class QmyDetectModelConfigDialog(QDialog):
    detect_model_configActionEnable = pyqtSignal(bool)
    change_model_config = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_DetectModelConfigDialog()
        self.ui.setupUi(self)
        # 固定窗口
        self.setWindowFlag(Qt.WindowStaysOnTopHint)

        self.ui.setNowModelBtn.clicked.connect(self.set_now_model)
        self.ui.btnOk.clicked.connect(self.accepted)
        self.ui.btnCancle.clicked.connect(self.accepted)

    def setInitial(self):
        self.ui.modelcomboBox.clear()
        self.pt_list = os.listdir('./pt')
        self.pt_list = [file for file in self.pt_list if file.endswith('.pt')]
        self.pt_list.sort(key=lambda x: os.path.getsize('./pt/'+x))
        self.ui.modelcomboBox.addItems(self.pt_list)
        print(self.pt_list)

    def set_now_model(self):
        selected_model = self.ui.modelcomboBox.currentText()
        self.change_model_config.emit(selected_model)

    def sendConfigToMainWindow(self):
        return ("发送配置信息给主窗口")

    def showEvent(self, event):
        self.detect_model_configActionEnable.emit(False)
        super().showEvent(event)
        print("显示对话框")

    def closeEvent(self, event):
        self.detect_model_configActionEnable.emit(True)
        super().closeEvent(event)
        print("关闭对话框")

    def accepted(self):
        self.detect_model_configActionEnable.emit(True)
        selected_model = self.ui.modelcomboBox.currentText()
        self.change_model_config.emit(selected_model)

    def rejected(self):
        self.detect_model_configActionEnable.emit(True)
